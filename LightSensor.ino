/*
  Bar-graph sketch
 
 Turns on a series of LEDs proportional to a value of an analogue sensor.
 Eight LEDs are controlled but you can change the number of LEDs by changing
 the value of NbrLEDs and adding the pins to the ledPins array
 */
 
const int NbrLEDs = 8;
const int loopDelay = 100;
const int ledPins[] = { 5, 6, 7, 8, 9, 10, 11, 12};
const int photocellPin = A0;
int sensorValue = 0;        // value read from the sensor
int threshold = 50;
int ledLevel = 0;           // sensor value converted into LED 'bars'
int ambientLight = analogRead(photocellPin);      // Get the ambient light baseline value
int lastLight = ambientLight;

void setup() {
  for (int led = 0; led < NbrLEDs; led++)
  {
    pinMode(ledPins[led], OUTPUT);// make all the LED pins outputs
  }
  
  ledTrail(3);
}
 
void loop() {
   
  sensorValue = analogRead(photocellPin);
  ledLevel = map(sensorValue, ambientLight, 900, 0, NbrLEDs);  // map to the number of LEDs
  for (int led = 0; led < NbrLEDs; led++){ digitalWrite(ledPins[led],LOW); } // turn off all LEDs before running next loop
  for (int led = 0; led < NbrLEDs; led++)
  {
    if (led < ledLevel ) {
      digitalWrite(ledPins[led], HIGH);     // turn on pins less than the leve
    }
  }
  if (abs(sensorValue - lastLight) >= threshold){ ledFlash(3); }
  
  lastLight = sensorValue;
  delay(loopDelay);
}

void ledTrail(int iterations) {     // LED chaser animation
  for (int iteration = 0; iteration < iterations; iteration++) {
    for (int led = 0; led < NbrLEDs; led++){
      digitalWrite(ledPins[led],HIGH);
      delay(50);
    }
    for (int led = 0; led < NbrLEDs; led++){
      digitalWrite(ledPins[led],LOW);
      delay(50);
    }
  }
}

void ledBounce(int iterations) {      // LED bounce animation
  for (int iteration = 0; iteration < iterations; iteration++) {
    for (int led = 0; led < NbrLEDs; led++){
      digitalWrite(ledPins[led],HIGH);
      delay(50);
    }
    for (int led = NbrLEDs; led > 0; led--){
      digitalWrite(ledPins[led],LOW);
      delay(50);
    }
  }
}

void ledFlash(int iterations){
  for (int iteration = 0; iteration < iterations; iteration++) {
    for (int led = 0; led < NbrLEDs; led++){
      digitalWrite(ledPins[led],HIGH);
    }
    delay(100);
    for (int led = 0; led < NbrLEDs; led++){
      digitalWrite(ledPins[led],LOW);
    }
    delay(100);
  }
}
